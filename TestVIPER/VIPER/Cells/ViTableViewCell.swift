//
//  ViTableViewCell.swift
//  TestVIPER
//
//  Created by Сергей Вихляев on 06.11.2021.
//

import UIKit

class ViTableViewCell: UITableViewCell {

	@IBOutlet weak var infoLabel: UILabel!

	func configure(text: String) {
		infoLabel.text = text
	}

	override func prepareForReuse() {
		super.prepareForReuse()
		infoLabel.text = ""
	}
}
