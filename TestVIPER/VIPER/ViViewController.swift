//
//  ViViewController.swift
//  TestVIPER
//
//  Created by Сергей Вихляев on 06.11.2021.
//

import UIKit

protocol DisplayLogic: AnyObject {
	func display(_ viewModels: [String])
}

class ViViewController: UIViewController {

	var presenter: EventIntercepter?

	var viewModels: [String] = [] {
		didSet {
			tableView?.reloadData()
		}
	}

	@IBOutlet weak var tableView: UITableView?

	override func viewDidLoad() {
		super.viewDidLoad()

		tableView?.delegate = self
		tableView?.dataSource = self

		tableView?.register(UINib(nibName: String(describing: ViTableViewCell.self), bundle: nil),
						   forCellReuseIdentifier: String(describing: ViTableViewCell.self))
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		presenter?.requestData()
	}
}

extension ViViewController: DisplayLogic {
	func display(_ viewModels: [String]) {
		self.viewModels = viewModels
	}
}

// MARK: - UITableViewDelegate & UITableViewDataSource
extension ViViewController: UITableViewDelegate & UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		viewModels.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ViTableViewCell.self), for: indexPath) as! ViTableViewCell
		cell.configure(text: viewModels[indexPath.row])
		return cell
	}
}
