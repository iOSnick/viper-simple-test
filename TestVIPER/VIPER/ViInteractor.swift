//
//  ViInteractor.swift
//  TestVIPER
//
//  Created by Сергей Вихляев on 06.11.2021.
//

import Foundation

protocol BusinesLogic {
	func requestData()
}

class ViInteractor {

	weak var presenter: PresentationsLogic?

	let data = ["1","2","3","4","surprise"]
}

// MARK: - BusinesLogic
extension ViInteractor: BusinesLogic {
	func requestData() {
		presenter?.presentData(data)
	}
}
