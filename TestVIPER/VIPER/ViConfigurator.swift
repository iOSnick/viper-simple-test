//
//  ViConfigurator.swift
//  TestVIPER
//
//  Created by Сергей Вихляев on 06.11.2021.
//

import Foundation
import UIKit

class ViConfigurator {

	func config(view: UIViewController, navigationController: UINavigationController?) {
		guard let view = view as? ViViewController else { return }

		let presenter = ViPresenter()
		let interactor = ViInteractor()

		let router = ViRouter()
		router.navigationController = navigationController

		view.presenter = presenter
		presenter.view = view
		presenter.interactor = interactor
		presenter.router = router
		interactor.presenter = presenter
	}
}
