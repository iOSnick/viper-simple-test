//
//  ViPresenter.swift
//  TestVIPER
//
//  Created by Сергей Вихляев on 06.11.2021.
//

import Foundation

/// Логика
protocol EventIntercepter {
	func requestData()
}

/// Логика презентации
protocol PresentationsLogic: AnyObject {
	func presentData(_ data: [String])
}

class ViPresenter {

	weak var view: DisplayLogic?
	var interactor: BusinesLogic?
	var router: ViRouter?
}

extension ViPresenter: EventIntercepter {
	func requestData() {
		interactor?.requestData()
	}
}

extension ViPresenter: PresentationsLogic {
	func presentData(_ data: [String]) {
		let viewModels = data.map({ $0 })
		view?.display(viewModels)
	}
}
