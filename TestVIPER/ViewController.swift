//
//  ViewController.swift
//  TestVIPER
//
//  Created by Сергей Вихляев on 06.11.2021.
//

import UIKit

class ViewController: UIViewController {

	@IBAction func createVIPERModuleButtonDidTap(_ sender: Any) {

		// 1 Создать viewController
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		let viewController = storyboard.instantiateViewController(withIdentifier: "ViViewController") as! ViViewController

		// 2 Конфигурация viper-модуля
		ViConfigurator().config(view: viewController, navigationController: navigationController)

		// 3. Навигация
		navigationController?.pushViewController(viewController, animated: true)
	}
}

